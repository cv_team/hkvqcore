//
// Created by Pulsar on 2021/1/8.
//

#ifndef HKVQCORE_HKV_DRIVER_H
#define HKVQCORE_HKV_DRIVER_H
#include <HCNetSDK/HCNetSDK.h>
#include <hkvqcore/driver/hkv_PlayM4.h>
#include <hkvqcore/data_struct/data_struct.h>
#include <opencv2/opencv.hpp>
#include "opencv2/imgproc/imgproc_c.h"
namespace hkvqcore{
    namespace driver{
        using namespace cv;
        class HKCamDriver {
        private:
            std::string sIP;
            std::string UsrName;
            std::string PsW;
            int Port{};
            int channel{};
            int stream_type{};
        public:

            HKCamDriver(const std::string &sIP, const std::string &UsrName, const std::string &PsW, int Port,
                        int channel, int stream_type);

            HKCamDriver();

            ~HKCamDriver();

            void InitHKNetSDK();

            data_struct::CamHandle
            InitCamera(const char *ip_address, const char *username, const char *password, int port, int channel,
                       int stream_type);

            int ReleaseCamera();

            data_struct::CamHandle Init();

            cv::Mat Read(data_struct::CamHandle lRealPlayHandle);

            static  void CALLBACK ExceptionCallBack(DWORD dwType, LONG lUserID, LONG lHandle, void *pUser);

            static void CALLBACK DecCBFun(long nPort, char * pBuf, long nSize, FRAME_INFO * pFrameInfo, long nReserved1, long nReserved2);

            static void CALLBACK fRealDataCallBack(LONG lRealHandle, DWORD dwDataType, BYTE *pBuffer, DWORD dwBufSize, void *pUser);

            static void yv12toYUV(char *outYuv, char *inYv12, int width, int height, int widthStep);

        };
    }
}



#endif //HKVQCORE_HKV_DRIVER_H
