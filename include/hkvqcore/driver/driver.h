//
// Created by Pulsar on 2021/1/8.
//

#ifndef HKVQCORE_DRIVER_H
#define HKVQCORE_DRIVER_H

#include <string>
#include <hkvqcore/driver/hkv_driver.h>
#include <hkvqcore/driver/local_cam_driver.h>
#include <hkvqcore/data_struct//data_struct.h>
#include<pybind11/pybind11.h>
#include<pybind11/numpy.h>

namespace hkvqcore {
    namespace driver {
        class Driver {
        public:
            explicit Driver(int index);

            explicit Driver(const std::string &ip_address, const std::string &username, const std::string &password,
                            int Port = 8000, int channel = 1, int stream_type = 0);

            explicit Driver(const std::string &video_file);

            data_struct::CamHandle Init();

            pybind11::array_t<unsigned char> Read(long handel);

            cv::Mat ReadMat(long handel);

            void Release();

        private:
            bool IS_IPCAM = false;
            bool INIT_FLAG = false;
            Capture *cap;
            HKCamDriver *hkcap;
        };
    }
}

#endif //HKVQCORE_DRIVER_H
