//
// Created by Pulsar on 2021/1/8.
//

#ifndef HKVQCORE_LOCAL_CAM_DRIVER_H
#define HKVQCORE_LOCAL_CAM_DRIVER_H

#include <opencv2/opencv.hpp>
namespace hkvqcore {
    namespace driver {
        class Capture {
        protected:
            bool IS_FILE{};
            cv::VideoCapture *cap{};
            int index{};
            std::string video_file;
        public:
            explicit Capture();

            explicit Capture(int index);

            explicit Capture(std::string video_file);

            long Init();

            long Init(int index);

            long Init(std::string video_file);

            cv::Mat Read();

            void Release();
        };
    }
}


#endif //HKVQCORE_LOCAL_CAM_DRIVER_H
