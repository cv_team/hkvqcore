//
// Created by Pulsar on 2020/12/30.
//
#include <iostream>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <deque>
#include <hkvqcore/driver/hkv_driver.h>
#include <hkvqcore/data_struct/data_struct.h>

int main(int argc, char **argv) {
    int channel = 1;
    int stream_type = 0;
    for (int i = 0; i < argc; i += 1) {
        std::cout << argv[i] << std::endl;
    }
    if (argc > 1)
        channel = atoi(argv[1]);
    hkvqcore::driver::HKCamDriver cap;
    std::cout << "channel:" << channel << std::endl;
    cap.InitHKNetSDK();
    Sleep(100);
    hkvqcore::data_struct::CamHandle handel;
    handel = cap.InitCamera("192.168.50.73", "admin", "A220220220", 8000, channel, stream_type);
    Sleep(100);
    while (true) {
        cv::Mat frame = cap.Read(handel);
        cv::imshow("frame", frame);
        if (cv::waitKey(10) == 'q')break;
    }
    return 0;
}

