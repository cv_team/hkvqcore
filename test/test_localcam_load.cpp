//
// Created by Pulsar on 2020/12/30.
//
#include <iostream>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <deque>
#include <hkvqcore/driver/local_cam_driver.h>
#include <hkvqcore/data_struct/data_struct.h>

int main() {
    hkvqcore::driver::Capture capture(0);
    cv::Mat frame;
    capture.Init();
    while (true) {
        frame = capture.Read();
        cv::imshow("frame", frame);
        if (cv::waitKey(10) == 'q')break;
    }
    return 0;
}

