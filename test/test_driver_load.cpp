//
// Created by Pulsar on 2020/12/30.
//
#include <iostream>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <deque>
#include <opencv2/opencv.hpp>
#include <hkvqcore/driver/driver.h>

int main() {
    hkvqcore::driver::Driver driver("192.168.50.73", "admin", "A220220220", 8000, 1);
    driver.Init();
    while (true) {
        cv::Mat frame;
        driver.Read(0);
        if (cv::waitKey(10) == 'q') {
            break;
        }
    }

    return 0;
}

