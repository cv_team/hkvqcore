//
// Created by Pulsar on 2021/1/8.
//
#include <hkvqcore/driver/driver.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/chrono.h>
#include <pybind11/complex.h>
#include <pybind11/functional.h>

PYBIND11_MODULE(pyhkvqcore, module) {
    module.doc() = "pyhkvqcore plugin";
    using namespace hkvqcore::driver;
    pybind11::class_<hkvqcore::driver::Driver>(module, "Driver")
            .def(pybind11::init<int>())
            .def(pybind11::init<std::string, std::string, std::string, int, int, int>())
            .def("Init", &hkvqcore::driver::Driver::Init, "Init Webcam")
            .def("Read", &hkvqcore::driver::Driver::Read, "Read Stream")
            .def("Release", &hkvqcore::driver::Driver::Release, "Release Stream");
}


