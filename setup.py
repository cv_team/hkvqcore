import logging
from setuptools import setup, find_packages, Extension
from Cython.Build import cythonize
import os

file_root = os.path.dirname(os.path.realpath(__file__))
source_root = file_root + '/src'
source_dirs = os.listdir(source_root)
source_files = []
for source_dir in source_dirs:
    source_dir = os.path.join(source_root, source_dir)
    files = os.listdir(source_dir)
    for file in files:
        logging.warning(file)
        source_files.append(os.path.join(source_dir, file))
setup(
    name="pyhkvqcore",
    version="1.0",
    keywords=("HCNetSDK",),
    description="An Web Camera SDK",
    license="Apache1.0 Licence",
    author="PulsarV",
    author_email="zhijiatao@outlook.com",

    include_package_data=True,
    platforms="windows",
    data_files=[
        file_root + '/3rdparty/opencv/build/x64/vc15/bin/opencv_videoio_ffmpeg450_64.dll',
        file_root + '/3rdparty/opencv/build/x64/vc15/bin/opencv_videoio_msmf450_64.dll',
        file_root + '/3rdparty/opencv/build/x64/vc15/bin/opencv_world450.dll',
        file_root + '/3rdparty/HCNetSDK/lib/LocalXml.zip',
        file_root + '/3rdparty/HCNetSDK/lib/AudioRender.dll',
        file_root + '/3rdparty/HCNetSDK/lib/D3DCompiler_43.dll',
        file_root + '/3rdparty/HCNetSDK/lib/d3dx9_43.dll',
        file_root + '/3rdparty/HCNetSDK/lib/EagleEyeRender.dll',
        file_root + '/3rdparty/HCNetSDK/lib/GdiPlus.dll',
        file_root + '/3rdparty/HCNetSDK/lib/HCCore.dll',
        file_root + '/3rdparty/HCNetSDK/lib/HCNetSDK.dll',
        file_root + '/3rdparty/HCNetSDK/lib/HWDecode.dll',
        file_root + '/3rdparty/HCNetSDK/lib/libmmd.dll',
        file_root + '/3rdparty/HCNetSDK/lib/MP_Render.dll',
        file_root + '/3rdparty/HCNetSDK/lib/PlayCtrl.dll',
        file_root + '/3rdparty/HCNetSDK/lib/SuperRender.dll',
        file_root + '/3rdparty/HCNetSDK/lib/YUVProcess.dll',
    ],

    ext_modules=cythonize([
        Extension("pyhkvqcore", source_files,
                  language="c++",
                  extra_compile_args=[
                      '/DWIN32',
                      '/D_WINDOWS',
                      '/GR',
                      '/EHsc',
                  ],
                  libraries=[
                      "opencv_world450",
                      "GdiPlus",
                      "HCCore",
                      "HCNetSDK",
                      "PlayCtrl",
                  ],
                  include_dirs=[
                      file_root + '/include',
                      file_root + '/3rdparty/opencv/build/include',
                      file_root + '/3rdparty/pybind11/include',
                      file_root + '/3rdparty/HCNetSDK/include',
                  ],
                  library_dirs=[
                      file_root + '/3rdparty/opencv/build/x64/vc15/lib',
                      file_root + '/3rdparty/HCNetSDK/lib',
                  ]
                  ),
    ]),
    scripts=[],
)
