编译安装
```bash
python setup.py install
```
加载海康威视摄像头例子
```python
# TODO:海康威视摄像头读取
import os, sys
import cv2
import pyhkvqcore as pqc
if __name__ == '__main__':
    # TODO:实例化摄像头驱动
    driver = pqc.Driver(0)
    # TODO:初始化摄像头驱动(必须)
    driver.Init()
    while True:
        # TODO:读取图像
        frame = driver.Read()
        cv2.imshow('frame', frame)
        if cv2.waitKey(1) == 'q':
            break
    # TODO:释放摄像头
    driver.Release()
```
加载本地摄像头例子
```python
# -*- coding: utf-8 -*-
# TODO:海康威视摄像头读取
import os, sys
import cv2

# TODO:配置cpython环境 若已经安装则无需配置此环境
import pyhkvqcore as pqc
if __name__ == '__main__':
    # TODO:实例化摄像头驱动
    driver = pqc.Driver(0)
    # TODO:初始化摄像头驱动(必须)
    driver.Init()
    while True:
        # TODO:读取图像
        frame = driver.Read()
        cv2.imshow('frame', frame)
        if cv2.waitKey(1) == 'q':
            break
    # TODO:释放摄像头
    driver.Release()
```