# -*- coding: utf-8 -*-
# TODO:海康威视摄像头读取
import os, sys
import cv2

try:
    import pyhkvqcore as pqc
except:
    # TODO:配置cpython环境 若已经安装则无需配置此环境
    dir_path = os.path.dirname(os.path.realpath(__file__))
    lib_path = dir_path + '/../cmake-build-debug/bin'
    sys.path.append(lib_path)
    os.environ['PATH'] = os.environ['PATH'] + ';' + lib_path + ';'
    # TODO:导入cpython摄像头驱动包
    import pyhkvqcore as pqc

if __name__ == '__main__':
    # TODO:实例化摄像头驱动
    driver = pqc.Driver(0)
    # TODO:初始化摄像头驱动(必须)
    handel = driver.Init()
    print('Camera Handel:', handel)
    while True:
        # TODO:读取图像
        frame = driver.Read(0)
        cv2.imshow('frame', frame)
        if cv2.waitKey(1) == 'q':
            break
    # TODO:释放摄像头
    driver.Release()
