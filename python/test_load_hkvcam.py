# -*- coding: utf-8 -*-
# TODO:本地摄像头读取
import os, sys
import cv2

from pyhkvqcore import pyhkvqcore as pqc

if __name__ == '__main__':
    # TODO:实例化摄像头驱动
    driver = pqc.Driver("192.168.50.73", 'admin', 'A220220220', 8000, 1, 1)
    # TODO:初始化摄像头驱动(必须)
    handel = driver.Init()
    print('Camera Handel:', handel)
    if handel == -1:
        exit("Open Camera Error")
    while True:
        # TODO:读取图像
        frame = driver.Read(handel)
        cv2.imshow('frame', frame)
        if cv2.waitKey(1) == 'q':
            break
    # TODO:释放摄像头
    driver.Release()
