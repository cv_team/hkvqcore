if (${WITH_HKVQCORE_TEST} STREQUAL "ON")
    file(GLOB HKVQCORE_TEST_SOURCES_FILES ${HKVQCORE_TEST_DIR}/*.cpp)
    foreach (HKVQCORE_TEST_EXE ${HKVQCORE_TEST_SOURCES_FILES})
        get_filename_component(HKVQCORE_TEST_NAME ${HKVQCORE_TEST_EXE} NAME_WE)
        message(STATUS "TEST_EXE:${HKVQCORE_TEST_NAME}")
        add_executable(${HKVQCORE_TEST_NAME} ${HKVQCORE_TEST_EXE})
        IF (CMAKE_SYSTEM_NAME MATCHES "Linux")
            target_link_libraries(${HKVQCORE_TEST_NAME}
                    hkvqcore
                    ${HKVQCORE_THIRD_LIBS}
                    )
        ELSEIF (CMAKE_SYSTEM_NAME MATCHES "Windows")
            target_link_libraries(${HKVQCORE_TEST_NAME}
                    libhkvqcore
                    ${HKVQCORE_THIRD_LIBS}
                    )
            target_link_libraries(${HKVQCORE_TEST_NAME}
                    libhkvqcore_static
                    )
        ENDIF (CMAKE_SYSTEM_NAME MATCHES "Linux")
    endforeach ()
endif ()