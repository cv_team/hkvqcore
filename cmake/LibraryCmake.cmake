include_directories(${HKVQCORE_INCLUDE_DIR})
file(GLOB HKVQCORE_MODULES_SOURCES_DIR ${HKVQCORE_SOURCES_DIR}/*)
file(GLOB HKVQCORE_MODULES_HEADERS_DIR ${HKVQCORE_INCLUDE_DIR})

set(HKVQCORE_SOURCES "")
set(HKVQCORE_THIRD_LIBS
        ${PYTHON_LIBRARIES}
        ${HCNetSDK_LIBS}
        ${OpenCV_LIBS}
        )
foreach (HKVQCORE_MODULE ${HKVQCORE_MODULES_SOURCES_DIR})
    message(STATUS "HKVQCORE_MODULE :${HKVQCORE_MODULE}")
    file(GLOB HKVQCORE_MODULE_SOURCES ${HKVQCORE_MODULE}/*.cpp)
    foreach (HKVQCORE_MODULE_SOURCE_FILE ${HKVQCORE_MODULE_SOURCES})
        message(STATUS "HKVQCORE_MODULE_SOURCE_FILE :${HKVQCORE_MODULE_SOURCE_FILE}")
        LIST(APPEND HKVQCORE_SOURCES ${HKVQCORE_MODULE_SOURCE_FILE})
    endforeach ()
endforeach ()
LIST(LENGTH HKVQCORE_SOURCES SOURCES_LENGTH)
message(STATUS "SOURCES_LENGTH:${SOURCES_LENGTH}")


IF (CMAKE_SYSTEM_NAME MATCHES "Linux")
    MESSAGE(STATUS "current platform: Linux ")
    add_library(hkvqcore SHARED ${HKVQCORE_SOURCES})
    target_link_libraries(HKVQCORE
            ${HKVQCORE_THIRD_LIBS}
            )
    foreach (HKVQCORE_THIRD_LIB ${HKVQCORE_THIRD_LIBS})
        message(STATUS "HKVQCORE_THIRD_LIB:${HKVQCORE_THIRD_LIB}")
    endforeach ()
    pybind11_add_module(pyhkvqcore ${HKVQCORE_SOURCES})
    #    target_link_libraries(pyhkvqcore
    #            ${RAINBOW_THIRD_LIBS}
    #            )
ELSEIF (CMAKE_SYSTEM_NAME MATCHES "Windows")
    add_library(libhkvqcore SHARED ${HKVQCORE_SOURCES})
    target_link_libraries(libhkvqcore
            ${HKVQCORE_THIRD_LIBS}
            )
    add_library(libhkvqcore_static STATIC ${HKVQCORE_SOURCES})
    target_link_libraries(libhkvqcore_static
            ${HKVQCORE_THIRD_LIBS}
            )
    pybind11_add_module(pyhkvqcore ${HKVQCORE_SOURCES})
    target_link_libraries(pyhkvqcore PRIVATE ${HKVQCORE_THIRD_LIBS})

    set_target_properties(libhkvqcore_static PROPERTIES OUTPUT_NAME "librainbow")
    set_target_properties(libhkvqcore PROPERTIES CLEAN_DIRECT_OUTPUT 1)
    set_target_properties(libhkvqcore_static PROPERTIES CLEAN_DIRECT_OUTPUT 1)
ENDIF (CMAKE_SYSTEM_NAME MATCHES "Linux")

