set(HCNetSDK_DIR ${PROJECT_SOURCE_DIR}/3rdparty/HCNetSDK)
set(HCNetSDK_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/3rdparty/HCNetSDK/include)
set(HCNetSDK_LIBRARY_DIR ${PROJECT_SOURCE_DIR}/3rdparty/HCNetSDK/lib)
set(HCNetSDK_LIBS
        GdiPlus.lib
        HCCore.lib
        HCNetSDK.lib
        PlayCtrl.lib
        )

include_directories(
        ${HCNetSDK_INCLUDE_DIR}
)
link_directories(
        ${HCNetSDK_LIBRARY_DIR}
)
